from __future__ import print_function

import argparse
import logging
import random
import time
import cv2
import numpy as np

from carla import image_converter
from carla.client import make_carla_client
from carla.sensor import Camera, Lidar
from carla.settings import CarlaSettings
from carla.tcp import TCPConnectionError
from carla.util import print_over_same_line


WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
MINI_WINDOW_WIDTH = 320
MINI_WINDOW_HEIGHT = 180

numero_de_episodios = 50
numero_de_frame = 5000

# Parametros para la funcion de Lucas Kanade
lk_params = dict(winSize=(15, 15),
                 maxLevel=3,
                 criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

def obtencionDatos(args):
    iter = 0

    with make_carla_client(args.host, args.port) as client:
        print('CarlaClient connected')

        for episode in range(0, numero_de_episodios):
            # Start a new episode.

            if args.settings_filepath is None:
                # Create a CarlaSettings object. This object is a wrapper around
                # the CarlaSettings.ini file. Here we set the configuration we
                # want for the new episode.
                settings = CarlaSettings()
                settings.set(
                    SynchronousMode=True,
                    SendNonPlayerAgentsInfo=False,
                    NumberOfVehicles=0,
                    NumberOfPedestrians=0,
                    WeatherId=random.choice([1, 3, 7, 8, 14]),
                    QualityLevel='Low')
                settings.randomize_seeds()
                configuracionCamaras(settings)

                # Together with the measurements, the server has sent the
                # control that the in-game autopilot would do this frame. We
                # can enable autopilot by sending back this control to the
                # server. We can modify it if wanted, here for instance we
                # will add some noise to the steer.
                scene = client.load_settings(settings)

                # Choose one player start at random.
                number_of_player_starts = len(scene.player_start_spots)
                player_start = random.randint(0, max(0, number_of_player_starts - 1))

                print('Starting new episode...')
                client.start_episode(player_start)

                for frame in range(0, numero_de_frame):

                    # Read the data produced by the server this frame.
                    measurements, sensor_data = client.read_data()


                    #Analisis de la imagen y el la obtencion de datos de entrenamiento.


                    imagenes(sensor_data, iter)
                    iter = iter + 1

                    #

                    control = measurements.player_measurements.autopilot_control
                    control.steer += random.uniform(-0.1, 0.1)
                    client.send_control(control)

def imagenes(sensor,iter):

    amarillo_A = np.array([220, 220, 0], dtype=np.uint8)
    amarillo_B = np.array([220, 220, 1], dtype=np.uint8)
    imagenRGB = sensor.get('CamaraRGB', None)
    imagenSeg = sensor.get('CamaraSegmentada', None)

    if imagenSeg is not None and imagenRGB is not None:

        imagenRGB = image_converter.to_rgb_array(imagenRGB)
        imagenSeg = image_converter.labels_to_cityscapes_palette(imagenSeg)
        imagenRGB = imagenRGB[100:150 + len(imagenRGB) - 150, 300:350 + (len(imagenRGB[0]) - 350)]
        imagenSeg = imagenSeg[100:150 + len(imagenSeg) - 150, 300:350 + (len(imagenSeg[0]) - 350)]
        #cv2.imshow("ca",imagenSeg)
        #cv2.waitKey(0)
        mascara = cv2.inRange(imagenSeg, amarillo_A, amarillo_B)
        mascara, contorno, _ = cv2.findContours(mascara, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        max_area = 0
        best_cnt = None


        for cnt in contorno:
            area = cv2.contourArea(cnt)
            if area > max_area:
                best_cnt = cnt
                max_area = area

        if max_area > 150:
            (x, y, w, h) = cv2.boundingRect(best_cnt)
            res = cv2.bitwise_or(imagenRGB, imagenRGB, mask=mascara)
            img = res[y:h + y, x:x + w]
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            cv2.imwrite('./salida/save' + str(iter) + '.tif', img)


def configuracionCamaras(settings):
    camaraRGB = Camera('CamaraRGB')
    camaraRGB.set_image_size(WINDOW_WIDTH, WINDOW_HEIGHT)
    camaraRGB.set_position(2.0, 0.0, 1.4)
    camaraRGB.set_rotation(0.0, 0.0, 0.0)
    settings.add_sensor(camaraRGB)
    camara_Segmentada = Camera('CamaraSegmentada',PostProcessing='SemanticSegmentation')
    camara_Segmentada.set_image_size(WINDOW_WIDTH, WINDOW_HEIGHT)
    camara_Segmentada.set_position(2.0, 0.0, 1.4)
    camara_Segmentada.set_rotation(0.0, 0.0, 0.0)
    settings.add_sensor(camara_Segmentada)

def main():
    argparser = argparse.ArgumentParser(description=__doc__)
    argparser.add_argument(
        '-v', '--verbose',
        action='store_true',
        dest='debug',
        help='print debug information')
    argparser.add_argument(
        '--host',
        metavar='H',
        default='localhost',
        help='IP of the host server (default: localhost)')
    argparser.add_argument(
        '-p', '--port',
        metavar='P',
        default=2000,
        type=int,
        help='TCP port to listen to (default: 2000)')
    argparser.add_argument(
        '-a', '--autopilot',
        action='store_true',
        help='enable autopilot')
    argparser.add_argument(
        '-l', '--lidar',
        action='store_true',
        help='enable Lidar')
    argparser.add_argument(
        '-q', '--quality-level',
        choices=['Low', 'Epic'],
        type=lambda s: s.title(),
        default='Epic',
        help='graphics quality level, a lower level makes the simulation run considerably faster.')
    argparser.add_argument(
        '-i', '--images-to-disk',
        action='store_true',
        dest='save_images_to_disk',
        help='save images (and Lidar data if active) to disk')
    argparser.add_argument(
        '-c', '--carla-settings',
        metavar='PATH',
        dest='settings_filepath',
        default=None,
        help='Path to a "CarlaSettings.ini" file')

    args = argparser.parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=log_level)

    logging.info('listening to server %s:%s', args.host, args.port)

    args.out_filename_format = '_out/episode_{:0>4d}/{:s}/{:0>6d}'

    while True:
        try:
            obtencionDatos(args)
            print('Done.')
            return

        except TCPConnectionError as error:
            logging.error(error)
            time.sleep(1)
