from os import listdir
from os.path import isfile, join
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split
import cv2
import EntrenamientoSeñales.RecogidaDATASET as MS
from sklearn.neural_network import MLPClassifier


S30 = [ar for ar in listdir("./Senal30") if isfile(join("./Senal30", ar))]
S60 = [ar for ar in listdir("./Senal60") if isfile(join("./Senal60", ar))]
S90 = [ar for ar in listdir("./Senal90") if isfile(join("./Senal90", ar))]
RD = [ar for ar in listdir("./Ruido") if isfile(join("./Ruido", ar))]
SR = [ar for ar in listdir("./SemaforosRojos") if isfile(join("./SemaforosRojos", ar))]
SA = [ar for ar in listdir("./SemaforosAmarillos") if isfile(join("./SemaforosAmarillos", ar))]
SV = [ar for ar in listdir("./SemaforosVerdes") if isfile(join("./SemaforosVerdes", ar))]
S = [ar for ar in listdir("./Salida") if isfile(join("./Salida", ar))]

Rutas_dataset = ["./SemaforosVerdes/", "./SemaforosAmarillos/", "./SemaforosRojos/", "./Senal30/", "./Senal60/", "./Senal90/", "./Ruido/"]
Nombre = ["Ruido", "SemaforosVerdes", "SemaforosAmarillos", "SemaforosRojos", "Senal30", "Senal60", "Senal90", ]
ConjunotDataset = [SV, SA, SR, S30, S60, S90, RD]

NombreRed = "Modulo_Detencion_Trafico.sav"
Clase = [1, 2, 3, 4, 5, 6, 0] # SV,SA,SR,S30,S60,S90,R

def Entrenamiento():
    print("Inicio del entrenamiento")
    infoPixel = []
    clase = []
    for ConjuntoD in range(0, len(ConjunotDataset)):
        for file in ConjunotDataset[ConjuntoD]:
            Imagen = cv2.imread(Rutas_dataset[ConjuntoD] + file)
            print(Rutas_dataset[ConjuntoD] + file)
            Imagen = tratamiento(Imagen)
            infoPixel.append(obtencionDatos(Imagen))
            clase.append(Clase[ConjuntoD])

    x_train, x_test, y_train, y_test = train_test_split(infoPixel, clase, test_size=0.0001, random_state=27)
    clf = MLPClassifier(hidden_layer_sizes=(50,50, 50), max_iter=700, alpha=0.001, verbose=True, random_state=21,
                        tol=0.00000000001)
    clf.fit(x_train, y_train)

    joblib.dump(clf, NombreRed)

def tratamiento(Imagen):
    Imagen = cv2.cvtColor(Imagen, cv2.COLOR_RGB2GRAY)
    Imagen = cv2.resize(Imagen, (41, 41))
    return Imagen

def obtencionDatos(Imagen):
    X = []
    for ejeX in range(0, len(Imagen)):
        for ejeY in range(0, len(Imagen[ejeX])):
            X.append(Imagen[ejeX, ejeY])
    return X

def Zona_Testeo():
    for imagenTestR in S:
        ImagenTest = cv2.imread("./salida/" + imagenTestR)
        Im = ImagenTest
        ImagenTest = tratamiento(ImagenTest)
        redNN = joblib.load(NombreRed)
        x_test = [obtencionDatos(ImagenTest)]
        print(Nombre[redNN.predict(x_test)[0]] + " ----- " + imagenTestR)
        cv2.imshow("prueba", Im)
        cv2.waitKey(0)
        cv2.destroyAllWindows()



if __name__ == '__main__':
    print("-------------------- Modulo para el entrenamiento de la deteccion de señales y semaforos -----------------")
    print("Pulse 1 para realizar un entrenamiento de todo.")
    print("Pulse 2 para realizar una fase de test.")
    print("Pulse 3 para realizar una captura de dataset de entrenamiento.")
    print("Pulse 0 para salir")
    opc = -1

    while opc != 0:
        opc = int(input())
        if opc == 1: Entrenamiento()
        elif opc == 2: Zona_Testeo()
        elif opc == 3: MS.main()

    print("Salida")