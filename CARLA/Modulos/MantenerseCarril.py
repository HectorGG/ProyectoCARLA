import cv2
import numpy as np
import operator



#TAM. IMAGENES
#320, 180 ---> Juntas -> 640, 180

rosas_bajos = np.array([127, 63, 127], dtype=np.uint8)
rosas_altos = np.array([129, 65, 129], dtype=np.uint8)

lineas_bajos = np.array([49, 233, 156], dtype=np.uint8)
lineas_altos = np.array([51, 235, 158], dtype=np.uint8)
global var_estado
var_estado = None
global planificador
planificador = None

def alante(img):
    tam = img.shape

    maskD = cv2.inRange(img[:, int(tam[1]/2):tam[1]], rosas_bajos, rosas_altos)
    maskI = cv2.inRange(img[:, 0:int(tam[1]/2)], lineas_bajos, lineas_altos)
##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskD.shape
    auxI = np.zeros((y_im,x_im))
    auxD = np.zeros((y_im,x_im))
    conta = 0

    img_result = np.zeros((tam[0],tam[1]))
    Negativa = []
    Positiva = []
    for gray in [maskI,maskD]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image'+str(conta),edges)
        #cv2.waitKey(0)

        if conta == 0:
            r = 3
            te = 100
        else:
            r = 3
            te = 80
        lines = cv2.HoughLines(edges,r,np.pi/180,te)
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m < 0 and conta == 0:
                            Positiva.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1,y1),(x2,y2),(0,255,255),2)
                        if m > 0 and conta == 1:
                            Negativa.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1+x_img,y1),(x2+x_img,y2),(0,0,255),2)
                        
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        
        Negativa = sorted(Negativa, key=operator.itemgetter(3), reverse=True)
        Positiva = sorted(Positiva, key=operator.itemgetter(3))

        try:
            if conta == 0:
                if len(Positiva)== 0:
                    return -2
                mat = Positiva[0]
                if mat[4] > -0.3:
                    return -2
                suma = 0
            else:
                if len(Negativa) == 0:
                    return 2 #Curva derecha
                mat = Negativa[0]
                if mat[4] < 0.5:
                    return 2 #Curva derecha
                suma = x_img
            for i in [mat]:
                if i is not None:
                    pt1 = np.array([i[0]+suma, i[1]])
                    pt2 = np.array([i[2]+suma, i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
        except Exception as e:
            pass
        conta += 1
    if len(Positiva) == 0 and len(Negativa)== 0:
        return 3

    #cv2.imshow('ime',img_result)
    #cv2.waitKey(0)
    
    aux = np.argmax(img_result, axis=None)
    maximos = np.unravel_index(aux, img_result.shape)

    xmax = maximos[0]
    ymax = maximos[1]


    mitad = int(tam[1]/2)
    diff = ymax-mitad
    #print("Desviacion con respecto a mi posicion: " + str(diff) + " pixeles")
    perc = (diff*100)/mitad
    #print("Porcentaje de desvio: "+str(perc))
    
    
    #cv2.circle(img, (ymax,xmax), 5, (0, 255, 0), 1)

    #cv2.imshow('image',img)
    #cv2.waitKey(0)
    return perc/100

def interseccion(img):
    tam = img.shape
    maskI = cv2.inRange(img, lineas_bajos, lineas_altos)

    y_im, x_im = maskI.shape
    auxI = np.zeros((y_im,x_im))
    auxD = np.zeros((y_im,x_im))
    conta = 0

    img_result = np.zeros((tam[0],tam[1]))
    Negativa = []
    Positiva = []
    for gray in [maskI]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image'+str(conta),edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,5,np.pi/180,80)
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m < -0.5 or m > 0.5:
                            #cv2.line(img,(x1,y1),(x2,y2),(255,0,0),2)
                            Positiva.append([x1,y1,x2,y2, m])
                        
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        #cv2.imshow('image',img)
        #cv2.waitKey(0)
        

        if len(Positiva)== 0:
            return True
        else:
            return False

def interseccion_alante(img):
    tam = img.shape
    maskI = cv2.inRange(img[:, 0:int(tam[1]/2)], rosas_bajos, rosas_altos)

    maskD = cv2.inRange(img[:, int(tam[1]/2):tam[1]], rosas_bajos, rosas_altos)
##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskD.shape
    auxD = np.zeros((y_im,x_im))

    img_result = np.zeros((tam[0],tam[1]))
    conta = 0
    Negativa = []
    Positiva = []
    for gray in [maskD, maskI]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image'+str(conta),edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,3,np.pi/180,80)
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m > 0.5 and conta == 0:
                            Negativa.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1+x_img,y1),(x2+x_img,y2),(0,0,255),2)
                        if m < 0 and conta == 1:
                            Positiva.append([x1-x_img,y1,x2-x_img,y2, m])
                            #cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
                    except Exception as e:
                        pass
        except Exception as e:
            pass

        if len(Negativa) > 0:
            Negativa = sorted(Negativa, key=operator.itemgetter(3), reverse=True)
            hor = 50
        else:
            Negativa = sorted(Positiva, key=operator.itemgetter(3))
            hor = 55

        try:
            mat = Negativa[0]
            suma = x_img
            for i in [mat]:
                if i is not None:
                    pt1 = np.array([i[0]+suma, i[1]])
                    pt2 = np.array([i[2]+suma, i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
        except Exception as e:
            pass
        conta += 1
    #Linea Horizontal
    horizontal = np.ones(1000)*hor
    x = 0
    try:
        for v in horizontal:
            img_result[int(v)-1][int(x)] += 1
            img_result[int(v)][int(x)] += 1
            img_result[int(v)+1][int(x)] += 1
            x += 1
    except Exception as e:
        pass

    #cv2.imshow('ime',img_result)
    #cv2.waitKey(0)
    
    aux = np.argmax(img_result, axis=None)
    maximos = np.unravel_index(aux, img_result.shape)

    xmax = maximos[0]
    ymax = maximos[1]


    mitad = int(tam[1]/2)
    diff = ymax-mitad
    #print("Desviacion con respecto a mi posicion: " + str(diff) + " pixeles")
    perc = (diff*100)/mitad
    #print("Porcentaje de desvio: "+str(perc))
    
    
    cv2.circle(img, (ymax,xmax), 5, (0, 255, 0), 1)

    #cv2.imshow('image',img)
    #cv2.waitKey(0)
    return perc/100

def interseccion_derecha(img):
    tam = img.shape
    maskI = cv2.inRange(img, lineas_bajos, lineas_altos)
##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskI.shape

    img_result = np.zeros((tam[0],tam[1]))
    
    for gray in [maskI]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image',edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,3,np.pi/180,100)
        Negativa = []
        Positiva = []
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m <= 0:
                            Positiva.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1,y1),(x2,y2),(0,255,255),2)
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        
        Positiva = sorted(Positiva, key=operator.itemgetter(3), reverse=True)

        maxima_y = 100
        if len(Positiva) > 0:
            mat = Positiva[0]
            
            for i in [mat]:
                if i is not None:
                    if i[1] < maxima_y:
                        maxima_y = i[1]
                    pt1 = np.array([i[0], i[1]])
                    pt2 = np.array([i[2], i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
        else:
            mat = [0,0,0,0,0.9725]

    umbral = 95
    if maxima_y > umbral:
        #cv2.imshow('ime',img_result)
        #cv2.waitKey(0)

        pend = mat[4]
        #if pend < -0.7:
         #   pend = -0.7
        maximo = 0
        minimo = 0.02857

        print("INTER DER: " + str(pend))
        '''v = (pend - minimo) / (maximo - minimo)
    
        perc = (v*100)'''

        perc = (-0.403*pend) + 0.39194
        if perc > 0.35:
            perc = 0.35
        #print("Porcentaje de desvio: "+str(perc))


        #cv2.imshow('image',img)
        #cv2.waitKey(0)
        #if perc/100 > 0.3:
        #    perc = 30
    else:
        perc = 0

    return perc

def interseccion_izquierda(img):
    tam = img.shape
    maskI = cv2.inRange(img, lineas_bajos, lineas_altos)
##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskI.shape

    img_result = np.zeros((tam[0],tam[1]))
    
    for gray in [maskI]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image',edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,3,np.pi/180,100)
        Negativa = []
        Positiva = []
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m >= 0:
                            Positiva.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1,y1),(x2,y2),(0,255,255),2)
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        
        Positiva = sorted(Positiva, key=operator.itemgetter(3))

        y_maxima = 100
        if len(Positiva) >0:
            mat = Positiva[0]
            
            for i in [mat]:
                if i is not None:
                    if y_maxima > i[3]:
                        y_maxima = i[3]
                    pt1 = np.array([i[0], i[1]])
                    pt2 = np.array([i[2], i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
        else:
            mat = [0,0,0,0,0.456]

    #cv2.imshow('ime',img_result)
    #cv2.waitKey(0)
    umbral = 80
    if y_maxima > umbral:
        pend = mat[4]
        if pend > 0.33:
            pend = 0.33
        maximo = 0
        minimo = 0.02857

        print("INTER IZQ: " + str(pend))
        v = (pend - minimo) / (maximo - minimo)

        #perc = (v*-100)
        #print("Porcentaje de desvio: "+str(perc))

        perc = (1.19047 * pend) -0.5071

        if perc < -0.20:
            perc = -0.20
        #cv2.imshow('image',img)
        #cv2.waitKey(0)
        #if perc/100 < -0.2:
        #    perc = -20
    else:
        perc = 0

    return perc

def curva_derecha(img):
    tam = img.shape
    maskD = cv2.inRange(img[:, 0:int(tam[1]/2)], lineas_bajos, lineas_altos)

##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskD.shape
    auxD = np.zeros((y_im,x_im))

    img_result = np.zeros((tam[0],tam[1]))
    
    for gray in [maskD]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image'+str(conta),edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,3,np.pi/180,100)
        Negativa = []
        Positiva = []
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m < 0:
                            Negativa.append([x1,y1,x2,y2, m])
                            #cv2.line(img,(x1,y1),(x2,y2),(0,0,255),2)
                        
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        
        Negativa = sorted(Negativa, key=operator.itemgetter(3))

        try:
            mat = Negativa[0]
            suma = 0
            for i in [mat]:
                if i is not None:
                    pt1 = np.array([i[0]+suma, i[1]])
                    pt2 = np.array([i[2]+suma, i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
            #Linea Horizontal
            horizontal = np.ones(1000)*40
            x = 0
            for v in horizontal:
                img_result[int(v)-1][int(x)] += 1
                img_result[int(v)][int(x)] += 1
                img_result[int(v)+1][int(x)] += 1
                x += 1
        except Exception as e:
            pass

    #cv2.imshow('ime',img_result)
    #cv2.waitKey(0)
    
    aux = np.argmax(img_result, axis=None)
    maximos = np.unravel_index(aux, img_result.shape)

    xmax = maximos[0]
    ymax = maximos[1]


    mitad = int(tam[1]/2)
    diff = ymax-mitad
    #print("Desviacion con respecto a mi posicion: " + str(diff) + " pixeles")
    perc = (diff*100)/mitad
    #print("Porcentaje de desvio: "+str(perc))
    
    
    #cv2.circle(img, (ymax,xmax), 5, (0, 255, 0), 1)

    #cv2.imshow('image',img)
    #cv2.waitKey(0)
    return perc/100

def curva_izquierda(img):
    tam = img.shape
    maskD = cv2.inRange(img[:, int(tam[1]/2):tam[1]], rosas_bajos, rosas_altos)

##    cv2.imshow('image',maskD)
##    cv2.waitKey(0)
##    cv2.imshow('image',maskI)
##    cv2.waitKey(0)
    y_im, x_im = maskD.shape
    auxD = np.zeros((y_im,x_im))

    img_result = np.zeros((tam[0],tam[1]))
    
    for gray in [maskD]:
    
        edges = cv2.Canny(gray,50,150,apertureSize = 3)
        
        y_img, x_img = edges.shape
        
        #cv2.imshow('image'+str(conta),edges)
        #cv2.waitKey(0)

        lines = cv2.HoughLines(edges,4,np.pi/180,70)
        Negativa = []
        Positiva = []
        try:
            for s in range(len(lines)):
                for rho,theta in lines[s]:
                    a = np.cos(theta)
                    b = np.sin(theta)
                    x0 = a*rho
                    y0 = b*rho
                    x1 = int(x0 + 1000*(-b))
                    y1 = int(y0 + 1000*(a))
                    x2 = int(x0 - 1000*(-b))
                    y2 = int(y0 - 1000*(a))


                    #Pendiente: m = y2 - y1 / x2 - x1
                    try:
                        pt1 = np.array([x1, y1])
                        pt2 = np.array([x2, y2])
                        m = (y2 - y1) / (x2 - x1)
                        if m > 0:
                            Negativa.append([x1+x_img,y1,x2+x_img,y2, m])
                            #cv2.line(img,(x1+x_img,y1),(x2+x_img,y2),(0,0,255),2)
                        
                    except Exception as e:
                        pass
        except Exception as e:
            pass
        
        Negativa = sorted(Negativa, key=operator.itemgetter(3), reverse=True )

        try:
            mat = Negativa[0]
            suma = 0
            for i in [mat]:
                if i is not None:
                    pt1 = np.array([i[0]+suma, i[1]])
                    pt2 = np.array([i[2]+suma, i[3]])
                    valores = createLineIterator(pt1, pt2, img_result)
                    for v in valores:
                        try:
                            img_result[int(v[1])-1][int(v[0])-1] += 1
                            img_result[int(v[1])-1][int(v[0])] += 1
                            img_result[int(v[1])-1][int(v[0])+1] += 1
                            img_result[int(v[1])][int(v[0])-1] += 1
                            img_result[int(v[1])][int(v[0])] += 1
                            img_result[int(v[1])][int(v[0])+1] += 1
                            img_result[int(v[1])+1][int(v[0])-1] += 1
                            img_result[int(v[1])+1][int(v[0])] += 1
                            img_result[int(v[1])+1][int(v[0])+1] += 1
                        except Exception as e:
                            pass
            #Linea Horizontal
            horizontal = np.ones(1000)*40
            x = 0
            for v in horizontal:
                img_result[int(v)-1][int(x)] += 1
                img_result[int(v)][int(x)] += 1
                img_result[int(v)+1][int(x)] += 1
                x += 1
        except Exception as e:
            pass

##    cv2.imshow('ime',img_result)
##    cv2.waitKey(0)
    
    aux = np.argmax(img_result, axis=None)
    maximos = np.unravel_index(aux, img_result.shape)

    xmax = maximos[0]
    ymax = maximos[1]


    mitad = int(tam[1]/2)
    diff = ymax-mitad
    #print("Desviacion con respecto a mi posicion: " + str(diff) + " pixeles")
    perc = (diff*100)/mitad
    #print("Porcentaje de desvio: "+str(perc))
    
    
##    cv2.circle(img, (ymax,xmax), 5, (0, 255, 0), 1)
##
##    cv2.imshow('image',img)
##    cv2.waitKey(0)
    return perc/100


def createLineIterator(P1, P2, img):
    """
    Produces and array that consists of the coordinates and intensities of each pixel in a line between two points

    Parameters:
        -P1: a numpy array that consists of the coordinate of the first point (x,y)
        -P2: a numpy array that consists of the coordinate of the second point (x,y)
        -img: the image being processed

    Returns:
        -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])     
    """
   #define local variables for readability
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    #difference and absolute difference between points
    #used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    #predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa,dXa),3),dtype=np.float32)
    itbuffer.fill(np.nan)

    #Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X: #vertical line segment
       itbuffer[:,0] = P1X
       if negY:
           itbuffer[:,1] = np.arange(P1Y - 1,P1Y - dYa - 1,-1)
       else:
           itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)              
    elif P1Y == P2Y: #horizontal line segment
       itbuffer[:,1] = P1Y
       if negX:
           itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
       else:
           itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
    else: #diagonal line segment
       steepSlope = dYa > dXa
       if steepSlope:
           slope = dX.astype(np.float32)/dY.astype(np.float32)
           if negY:
               itbuffer[:,1] = np.arange(P1Y-1,P1Y-dYa-1,-1)
           else:
               itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)
           itbuffer[:,0] = (slope*(itbuffer[:,1]-P1Y)).astype(np.int) + P1X
       else:
           slope = dY.astype(np.float32)/dX.astype(np.float32)
           if negX:
               itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
           else:
               itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
           itbuffer[:,1] = (slope*(itbuffer[:,0]-P1X)).astype(np.int) + P1Y

    #Remove points outside of image
    colX = itbuffer[:,0]
    colY = itbuffer[:,1]
    itbuffer = itbuffer[(colX >= 0) & (colY >=0) & (colX<imageW) & (colY<imageH)]

    #Get intensities from img ndarray
    itbuffer[:,2] = img[itbuffer[:,1].astype(np.uint),itbuffer[:,0].astype(np.uint)]

    return itbuffer



def situacion(img):
    global var_estado
    global planificador
    #for ruta in range(15,1030):
    #img = cv2.imread('Segmentada/Seg ('+str(ruta+1)+').tiff')
    img2 = img.copy()
    dev = alante(img)
    nointerce = 0
#    print("\nImagen: "+str(ruta))
    if dev == 2 or dev == -2 or dev == 3:
        if interseccion(img2) == True or var_estado is not None:
            var_estado = planificador
            #print("Estoy en una interseccion")
            if planificador == 0:
                giro = interseccion_alante(img2)
                nointerce = -1
            if planificador == 1:
                giro = interseccion_derecha(img2)
                nointerce = 1
            if planificador == -1:
                giro = interseccion_izquierda(img2)
                nointerce = -1
        else:
            if dev == 2:
                #print("Esa imagen es una curva a la derecha")
                giro = curva_derecha(img2)
                nointerce = 2
            if dev == -2:
                #print("Esa imagen es una curva a la izquierda")
                giro = curva_izquierda(img2)
                nointerce = -2
    if dev != 2 and dev != -2 and dev != 3:
        var_estado = None

        giro = dev
    #print("Gira: "+str(giro) + "   p:" + str(planificador))
    return giro, nointerce


def setPlanificador (valor):
    global planificador
    planificador = valor


#situacion()

#alante()
#curva_derecha()
#curva_izquierda()
#interseccion_izquierda()
#interseccion_derecha()
#interseccion_alante()
#mantenerse_carril()
